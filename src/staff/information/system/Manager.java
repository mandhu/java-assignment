/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.information.system;

import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class Manager extends Admin{
    private double healthAllowance;
    private double bonusAllowance;

    public Manager(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, String workingHours, ArrayList<String> responsibilities, double healthAllowance, double bonusAllowance) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary, workingHours, responsibilities);
        this.healthAllowance = healthAllowance;
        this.bonusAllowance = bonusAllowance;
    }

    public double getHealthAllowance() {
        return healthAllowance;
    }

    public void setHealthAllowance(double healthAllowance) {
        this.healthAllowance = healthAllowance;
    }

    public double getBonusAllowance() {
        return bonusAllowance;
    }

    public void setBonusAllowance(double bonusAllowance) {
        this.bonusAllowance = bonusAllowance;
    }

    @Override
    public String toString() {
        return  super.toString() + 
                "\n healthAllowance=" + healthAllowance + 
                "\n bonusAllowance=" + bonusAllowance;
    }
    
    public double getTotalSalary() {
        return (this.getEmployeeBasicSalary() + this.healthAllowance + this.bonusAllowance);
    }
    
    
}

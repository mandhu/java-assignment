package staff.information.system;

import java.util.ArrayList;

public class Trainee extends Engineer{
    private ArrayList<String> accessAllowed = new ArrayList<String>();

    public Trainee(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, ArrayList<String> areasOfExpertise, ArrayList<String> accessAllowed) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary, areasOfExpertise);
        this.accessAllowed = accessAllowed;
    }

    public ArrayList<String> getAccessAllowed() {
        return accessAllowed;
    }

    public void setAccessAllowed(ArrayList<String> accessAllowed) {
        this.accessAllowed = accessAllowed;
    }

    @Override
    public String toString() {
        return  super.toString() + 
                "\n accessAllowed=" + accessAllowed;
    }
    
    
    
}

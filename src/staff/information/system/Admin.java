/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.information.system;

import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class Admin extends Employee {
    private String workingHours;
    private ArrayList<String> responsibilities = new ArrayList<String>();

    public Admin(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, String workingHours, ArrayList<String> responsibilities) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary);
        this.workingHours = workingHours;
        this.responsibilities = responsibilities;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public ArrayList<String> getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(ArrayList<String> responsibilities) {
        this.responsibilities = responsibilities;
    }

    
    @Override
    public String toString() {
        return  super.toString() +
                "\n workingHours=" + workingHours + 
                "\n responsibilities=" + responsibilities;
    }
    
    
    
}

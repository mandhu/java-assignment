
package staff.information.system;

import java.util.ArrayList;


public abstract class Engineer extends Employee {
    private ArrayList<String> areasOfExpertise = new ArrayList<String>();

    public Engineer(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, ArrayList<String> areasOfExpertise) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary);
        this.areasOfExpertise = areasOfExpertise;
    }

    public ArrayList<String> getAreasOfExpertise() {
        return areasOfExpertise;
    }

    public void setAreasOfExpertise(ArrayList<String> areasOfExpertise) {
        this.areasOfExpertise = areasOfExpertise;
    }

    @Override
    public String toString() {
        return  super.toString() + 
                "\n areasOfExpertise=" + areasOfExpertise;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.information.system;

import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class Response {
    public boolean status;
    public Employee employee;
    public ArrayList<Employee> employees;
    public String message;
    
    public Response() {
        
    }
    
    public Response(boolean status, String message) {
        this.status = status;
        this.message = message;
    }
    
    public Response(boolean status, Employee employee) {
        this.status = status;
        this.employee = employee;
    }
    
    public Response(boolean status, Employee employee, String message) {
        this.status = status;
        this.employee = employee;
        this.message = message;
    }

    public Response(boolean status, ArrayList<Employee> employees, String message) {
        this.status = status;
        this.employee = employee;
        this.employees = employees;
    }
    
    
    
}

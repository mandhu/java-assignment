package staff.information.system;

import java.util.ArrayList;

/**
 * JAVA 2 Assignment
 * @author Mamdhooh Mohamed
 */
public class BaseController {
    private static ArrayList<Employee> employeeStore = new ArrayList<Employee>();
       
    
    // retrieve all employee
    public static Response getAll() {
        return new Response(true, employeeStore , "All employee retrieve successfully");
    }
    
    
    // add employee
    public static Response save(Employee employee, String type) {

        if(employeeStore.add(employee)){
            return new Response(true, employee, type + "added succuessfully");
        }
        
        return new Response(false, employee, "Employee cannot be added");
    }
       
    
    // retrieve one employee by employeeId, if exist
    public static Response find(String employeeId) {
        
        if(isExist(employeeId)) {
            Employee employee = employeeStore.stream().filter(emp -> emp.getEmployeeId().equalsIgnoreCase(employeeId)).findFirst().get();
            return new Response(true, employee, "Employee retreive successfully");
        }
        
        return new Response(false, "Employee cannot be found");
    }
      
    
    // update employee details by employee id, if exist
    public static Response update(Employee employee, String employeeId) {
        
        if (findIndex(employeeId) != -1) {
            employeeStore.set(findIndex(employeeId), employee);
            return new Response(true, employee, "Employee updated successfully");
        }
        
        return new Response(false, employee, "Employee cannot be updated");
    }
    
    
    // delete employee if exist
    public static Response delete(Employee employee) {

        if(employeeStore.remove(employee)) {
            return new Response(true, "Employee delete successfully");
        }
        
        return new Response(false, "Employee cannot be deleted");
    }
    
    
    // check employee exist, if exits return index or -1
    private static int findIndex(String employeeId) {

        for(Employee emp: employeeStore){
            if (emp.getEmployeeId().equalsIgnoreCase(employeeId)) {
                return employeeStore.indexOf(emp);
            }
        }      
        return -1;
    }
    
    
    // check employee exist or not
    public static boolean isExist(String employeeId) {
        return employeeStore.stream().anyMatch(emp -> emp.getEmployeeId().equalsIgnoreCase(employeeId));
    }
    
}

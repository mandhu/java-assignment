/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.information.system;

import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class SeniorEngineer extends Engineer{
    private ArrayList<String> scheduledReports = new ArrayList<String>();
    private ArrayList<String> leadingTechnology = new ArrayList<String>();

    public SeniorEngineer(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, ArrayList<String> areasOfExpertise, ArrayList<String> scheduledReports, ArrayList<String> leadingTechnology) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary, areasOfExpertise);
        this.scheduledReports = scheduledReports;
        this.leadingTechnology = leadingTechnology;
    }

    public ArrayList<String> getScheduledReports() {
        return scheduledReports;
    }

    public void setScheduledReports(ArrayList<String> scheduledReports) {
        this.scheduledReports = scheduledReports;
    }

    public ArrayList<String> getLeadingTechnology() {
        return leadingTechnology;
    }

    public void setLeadingTechnology(ArrayList<String> leadingTechnology) {
        this.leadingTechnology = leadingTechnology;
    }

    @Override
    public String toString() {
        return  super.toString() + 
                "\n scheduledReports=" + scheduledReports + 
                "\n leadingTechnology=" + leadingTechnology;
    }
    
    
    
    
    
}

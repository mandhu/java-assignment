/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.information.system;

import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class JuniorEngineer extends Engineer{
    private String supervisor;
    private ArrayList<String> trainingSchedule = new ArrayList<String>();

    public JuniorEngineer(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, ArrayList<String> areasOfExpertise, String supervisor, ArrayList<String> trainingSchedule) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary, areasOfExpertise);
        this.supervisor = supervisor;
        this.trainingSchedule = trainingSchedule;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public ArrayList<String> getTrainingSchedule() {
        return trainingSchedule;
    }

    public void setTrainingSchedule(ArrayList<String> trainingSchedule) {
        this.trainingSchedule = trainingSchedule;
    }

    @Override
    public String toString() {
        return  super.toString() + 
                "\n supervisor=" + supervisor + 
                "\n trainingSchedule=" + trainingSchedule;
    }
}

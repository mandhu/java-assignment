/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.information.system;

import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class Secretary extends Admin{
    private double mealAllowance;

    public Secretary(double mealAllowance, String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary, String workingHours, ArrayList<String> responsibilities) {
        super(employeePicture, employeeId, employeeName, employeeAddress, employeePhoneNumber, employeeBasicSalary, workingHours, responsibilities);
        this.mealAllowance = mealAllowance;
    }

    public double getMealAllowance() {
        return mealAllowance;
    }

    public void setMealAllowance(double mealAllowance) {
        this.mealAllowance = mealAllowance;
    }

    @Override
    public String toString() {
        return  super.toString() + 
                "\n mealAllowance=" + mealAllowance;
    }
    
    public double getTotalSalary() {
        return (this.getEmployeeBasicSalary() + this.mealAllowance);
    }
    
}


package staff.information.system;

public abstract class Employee {
    private String employeePicture;
    private String employeeId;
    private String employeeName;
    private String employeeAddress;
    private String employeePhoneNumber;
    private double employeeBasicSalary;

    public Employee(String employeePicture, String employeeId, String employeeName, String employeeAddress, String employeePhoneNumber, double employeeBasicSalary) {
        this.employeePicture = employeePicture;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeAddress = employeeAddress;
        this.employeePhoneNumber = employeePhoneNumber;
        this.employeeBasicSalary = employeeBasicSalary;
    }

    public String getEmployeePicture() {
        return employeePicture;
    }

    public void setEmployeePicture(String employeePicture) {
        this.employeePicture = employeePicture;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public String getEmployeePhoneNumber() {
        return employeePhoneNumber;
    }

    public void setEmployeePhoneNumber(String employeePhoneNumber) {
        this.employeePhoneNumber = employeePhoneNumber;
    }

    public double getEmployeeBasicSalary() {
        return employeeBasicSalary;
    }

    public void setEmployeeBasicSalary(double employeeBasicSalary) {
        this.employeeBasicSalary = employeeBasicSalary;
    }

    @Override
    public String toString() {
        return  "\n employeePicture=" + employeePicture + 
                "\n employeeId=" + employeeId + 
                "\n employeeName=" + employeeName + 
                "\n employeeAddress=" + employeeAddress + 
                "\n employeePhoneNumber=" + employeePhoneNumber + 
                "\n employeeBasicSalary=" + employeeBasicSalary;
    }
    
    public double getTotalSalary() {
        return this.employeeBasicSalary;
    }
    
}
